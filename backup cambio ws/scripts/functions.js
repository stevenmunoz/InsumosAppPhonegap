 $(document).ready(function () {
    var height = window.innerHeight;
    height_fin = height + "px";
    $('#content').css('min-height', height_fin);
    var height2 = height - 70;
    height_fin2 = height2 + "px";
    $('#tabla_menu').css('min-height', height_fin2);
    
    setInterval("Tamanio()", 500);
});

 //Permite cargar un listado de departamentos en una etiqueta select
 //@param divContainer : id del div a cargar con la respuesta del webservice
 function cargarDepartamentos(divContainer) {
    MostrarDivCargando();
    var firstItem = "";
    var bhRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">" +
    "<soapenv:Body>" +
    "<tem:ObtenerDepartamentos/>" +
    "</soapenv:Body>" +
    "</soapenv:Envelope>";   
    $.ajax({
        type: "POST",
        url: "http://190.60.251.18/Insumos/ServicioInsumos.svc",
        data: bhRequest,
        contentType: "text/xml",
        dataType: "text",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("SOAPAction", "http://tempuri.org/IServicioInsumos/ObtenerDepartamentos");    
        },
        success: function(data) {

            xmlDoc = getXmlDoc(data);   
            var nodes = xmlDoc.getElementsByTagName("DepartamentoDTO");
            count = nodes.length;

            if (count > 0) {

                for (var i = 0; i < count; i++) {
                var name = xmlDoc.getElementsByTagName("NombreDepartamento")[i].childNodes[0].nodeValue;
                var code = xmlDoc.getElementsByTagName("IdDepartamento")[i].childNodes[0].nodeValue;
                //Asignamos el primer valor retornado para popular los municipios
                firstItem = firstItem === "" ? code : firstItem;
                $('#'+divContainer).append('<option value='+code+'>'+name+'</option>');
                }
                cargarMunicipio(firstItem, 'select_mun');

                $(data).find("ObtenerDepartamentosResponse").each(function () {
                        //console.log("Resulset " + $(this).find("ObtenerDepartamentosResult").text());
                        OcultarDivCargando();
                });
            }
            else {
                
                OcultarDivCargando();
            }
            
        },
        error: function (xhr, status, error) {
            console.log(error);
        }
    });
}

 //Permite cargar un listado de municipios en una etiqueta select
 //@param idDpto : id del departamento
 //@param divContainer : id del div a cargar con la respuesta del webservice
function cargarMunicipio(idDpto, divContainer) 
{   
    MostrarDivCargando();
    $('#'+divContainer).empty();

    var bhRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">" +
    "<soapenv:Body>" +
    "<tem:ObtenerMunicipiosPorDepartamento>" +
    "<tem:idDepartamento>"+idDpto+"</tem:idDepartamento>" +
    "</tem:ObtenerMunicipiosPorDepartamento>" +
    "</soapenv:Body>" +
    "</soapenv:Envelope>";   
    $.ajax({
        type: "POST",
        url: "http://190.60.251.18/Insumos/ServicioInsumos.svc",
        data: bhRequest,
        contentType: "text/xml",
        dataType: "text",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("SOAPAction", "http://tempuri.org/IServicioInsumos/ObtenerMunicipiosPorDepartamento");    
        },
        success: function(data) {

            xmlDoc = getXmlDoc(data);   
            var nodes = xmlDoc.getElementsByTagName("MunicipioDTO");
            count = nodes.length;

            if (count > 0) {

                for (var i = 0; i < count; i++) {
                    var name = xmlDoc.getElementsByTagName("NombreMunicipio")[i].childNodes[0].nodeValue;
                    var code = xmlDoc.getElementsByTagName("IdMunicipio")[i].childNodes[0].nodeValue;
                    $('#'+divContainer).append('<option value='+code+'>'+name+'</option>');
                }
                
                $(data).find("ObtenerMunicipiosPorDepartamentoResponse").each(function () {
                        //console.log("Resulset " + $(this).find("ObtenerDepartamentosResult").text());
                        OcultarDivCargando();
                });
            } else {

                OcultarDivCargando();
            }
        },
        error: function (xhr, status, error) {
            console.log(error);
        }
    });
}

 //Permite cargar un listado de tipos de insumo en una etiqueta select
 //@param divContainer : id del div a cargar con la respuesta del webservice
function cargarTiposInsumo (divContainer) 
{
    MostrarDivCargando();
    var firstItem = "";
    var bhRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">" +
    "<soapenv:Body>" +
    "<tem:ObtenerTiposDeInsumo/>" +
    "</soapenv:Body>" +
    "</soapenv:Envelope>";   
    $.ajax({
        type: "POST",
        url: "http://190.60.251.18/Insumos/ServicioInsumos.svc",
        data: bhRequest,
        contentType: "text/xml",
        dataType: "text",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("SOAPAction", "http://tempuri.org/IServicioInsumos/ObtenerTiposDeInsumo");    
        },
        success: function(data) {

            xmlDoc = getXmlDoc(data);   
            var nodes = xmlDoc.getElementsByTagName("TipoInsumoDTO");
            count = nodes.length;

            if (count > 0) {

                for (var i = 0; i < count; i++) {
                    var name = xmlDoc.getElementsByTagName("NombreTipoInsumo")[i].childNodes[0].nodeValue;
                    var code = xmlDoc.getElementsByTagName("IdTipoInsumo")[i].childNodes[0].nodeValue;
                     //Asignamos el primer valor retornado para popular los tipos de producto
                    firstItem = firstItem === "" ? code : firstItem;
                    $('#'+divContainer).append('<option value='+code+'>'+name+'</option>');
                }
                cargarTiposProducto(firstItem, 'select_tipo_producto');

                $(data).find("ObtenerTiposDeInsumoResponse").each(function () {
                        //console.log("Resulset " + $(this).find("ObtenerDepartamentosResult").text());
                    OcultarDivCargando();
                });
            }
            else {
                
                OcultarDivCargando();
            }

        },
        error: function (xhr, status, error) {
            console.log(error);
        }
    });
}

 //Permite cargar un listado de tipos de insumo en una etiqueta select
 //@param idTipoInsumo : id del tipo de insumo
 //@param divContenedor : id del div a cargar con la respuesta del webservice
function cargarTiposProducto(idTipoInsumo, divContainer) 
{
    $('#'+divContainer).empty();
    MostrarDivCargando();
    var bhRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">" +
    "<soapenv:Body>" +
    "<tem:ObtenerTiposDeProductoPorTipoInsumo>" +
    "<tem:idTipoInsumo>"+idTipoInsumo+"</tem:idTipoInsumo>" +
    "</tem:ObtenerTiposDeProductoPorTipoInsumo>" +
    "</soapenv:Body>" +
    "</soapenv:Envelope>";   
    $.ajax({
        type: "POST",
        url: "http://190.60.251.18/Insumos/ServicioInsumos.svc",
        data: bhRequest,
        contentType: "text/xml",
        dataType: "text",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("SOAPAction", "http://tempuri.org/IServicioInsumos/ObtenerTiposDeProductoPorTipoInsumo");    
        },
        success: function(data) {

            xmlDoc = getXmlDoc(data);   
            var nodes = xmlDoc.getElementsByTagName("TipoProductoDTO");
            count = nodes.length;

            if (count > 0 ) {

                for (var i = 0; i < count; i++) {
                    var name = xmlDoc.getElementsByTagName("NombreTipoProducto")[i].childNodes[0].nodeValue;
                    var code = xmlDoc.getElementsByTagName("IdTipoProducto")[i].childNodes[0].nodeValue;
                    $('#'+divContainer).append('<option value='+code+'>'+name+'</option>');
                }
                    
                $('#'+divContainer).prepend("<option value='0' selected='selected'>Todos</option>");

                $(data).find("ObtenerTiposDeProductoPorTipoInsumoResponse").each(function () {
                        //console.log("Resulset " + $(this).find("ObtenerDepartamentosResult").text());
                        OcultarDivCargando();
                });
            }
            else {
                
                OcultarDivCargando();
            }
            
        },
        error: function (xhr, status, error) {
            console.log(error);
        }
    });
}

//Permite cargar un listado de tipos de insumo en una etiqueta select
//@param idTipoInsumo : id del tipo de insumo
//@param divContenedor : id del div a cargar con la respuesta del webservice
function cargarTiposProductoReporte(idTipoInsumo, divContainer) {
    $('#' + divContainer).empty();
    MostrarDivCargando();
    var bhRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">" +
    "<soapenv:Body>" +
    "<tem:ObtenerTiposDeProductoPorTipoInsumo>" +
    "<tem:idTipoInsumo>" + idTipoInsumo + "</tem:idTipoInsumo>" +
    "</tem:ObtenerTiposDeProductoPorTipoInsumo>" +
    "</soapenv:Body>" +
    "</soapenv:Envelope>";
    $.ajax({
        type: "POST",
        url: "http://190.60.251.18/Insumos/ServicioInsumos.svc",
        data: bhRequest,
        contentType: "text/xml",
        dataType: "text",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("SOAPAction", "http://tempuri.org/IServicioInsumos/ObtenerTiposDeProductoPorTipoInsumo");
        },
        success: function (data) {

            xmlDoc = getXmlDoc(data);
            var nodes = xmlDoc.getElementsByTagName("TipoProductoDTO");
            count = nodes.length;

            if (count > 0) {

                for (var i = 0; i < count; i++) {
                    var name = xmlDoc.getElementsByTagName("NombreTipoProducto")[i].childNodes[0].nodeValue;
                    var code = xmlDoc.getElementsByTagName("IdTipoProducto")[i].childNodes[0].nodeValue;
                    $('#' + divContainer).append('<option value=' + code + '>' + name + '</option>');
                }

                $(data).find("ObtenerTiposDeProductoPorTipoInsumoResponse").each(function () {
                    //console.log("Resulset " + $(this).find("ObtenerDepartamentosResult").text());
                    OcultarDivCargando();
                });
            }
            else {

                OcultarDivCargando();
            }

        },
        error: function (xhr, status, error) {
            console.log(error);
        }
    });
}

function cargarInsumosCombo(idMunicipio, idTipoProducto, divContainer) 
{
    $('#'+divContainer).empty();
    MostrarDivCargando();
    var bhRequest = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">" +
        "<soapenv:Body>" +
        "<tem:ObtenerInsumos>" +
        "<tem:idMunicipio>"+idMunicipio+"</tem:idMunicipio>" +
        "<tem:idTipoProducto>"+idTipoProducto+"</tem:idTipoProducto>" +
        "</tem:ObtenerInsumos>" +
        "</soapenv:Body>" +
        "</soapenv:Envelope>"; 
    $.ajax({
        type: "POST",
        url: "http://190.60.251.18/Insumos/ServicioInsumos.svc",
        data: bhRequest,
        contentType: "text/xml",
        dataType: "text",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("SOAPAction", "http://tempuri.org/IServicioInsumos/ObtenerInsumos");    
        },
        success: function(data) {

            xmlDoc = getXmlDoc(data);   
            var nodes = xmlDoc.getElementsByTagName("InsumoDTO");
            count = nodes.length;

            if (count > 0 ) {

                for (var i = 0; i < count; i++) {
                    alert(i);
                    var name = xmlDoc.getElementsByTagName("TipoProducto")[i].childNodes[0].nodeValue;
                    var code = xmlDoc.getElementsByTagName("IdInsumo")[i].childNodes[0].nodeValue;
                    $('#'+divContainer).append('<option value='+code+'>'+name+'</option>');
                }
                    
                //$('#'+divContainer).prepend("<option value='0' selected='selected'>Todos</option>");

                $(data).find("ObtenerInsumosResponse").each(function () {
                        //console.log("Resulset " + $(this).find("ObtenerDepartamentosResult").text());
                        OcultarDivCargando();
                });
            }
            else {
                
                OcultarDivCargando();
            }
            
        },
        error: function (xhr, status, error) {
            console.log(error);
        }
    });
}

//Return xmlDoc object
function getXmlDoc (data) {

    try { 
        
        //Internet Explorer
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async = "false";
        xmlDoc.loadXML(data);

    } catch (e) {
        try { 

            // Firefox, Mozilla, Opera, etc.
            parser = new DOMParser();
            xmlDoc = parser.parseFromString(data, "text/xml");
        }
        catch (e) {
            console.log(e.message);
            return;
        }
    }
    return xmlDoc;
}

//Ocultar Div cargando...
function OcultarDivCargando(data) {
    $('#loading').css("display", "none");
}

//Mostrar Div cargando...
function MostrarDivCargando(data) {
    $('#loading').css("display", "block");
}

//Adaptar el tamaño de la aplicación a cualquier pantalla
function Tamanio() {
    var height = window.innerHeight - $('.footer').height();
    height_fin = height + "px";
    $('#content').css('min-height', height_fin);
    var height2 = height - 70 - $('.footer').height();
    height_fin2 = height2 + "px";
    $('#tabla_menu').css('min-height', height_fin2);
}